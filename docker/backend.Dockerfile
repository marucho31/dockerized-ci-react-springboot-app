FROM maven:3.8-openjdk-11-slim AS builder
WORKDIR /app
COPY ./backend/.mvn/ .mvn
COPY ./backend/mvnw backend/pom.xml ./
RUN mvn dependency:go-offline
COPY ./backend/src/ ./src
RUN mvn clean package --batch-mode -DskipTests

FROM openjdk:11-jre-slim-buster
COPY --from=builder /app/target/*.jar ./app.jar
ENTRYPOINT ["java", "-jar", "app.jar"]
EXPOSE 8080